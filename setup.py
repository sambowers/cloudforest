from setuptools import setup

setup(name='cloudforest',
      packages = ['cloudforest'],
      version='0.1',
      description='Testing the Water Cloud Model for modelling AGB in dry forest with L-band radar.',
      url='https://bitbucket.org/sambowers/cloudforest',
      author='Samuel Bowers',
      author_email='sam.bowers@ed.ac.uk',
      license='GNU General Public License',
      zip_safe=False)

