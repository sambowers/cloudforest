# Run through entire processing chain

import matplotlib as mpl
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import numpy as np
import os
import pandas as pd
import scipy.stats

import pdb

import cloudforest.interpolate
import cloudforest.IO
import cloudforest.SM
import cloudforest.models


def loadData(alos_dir, plot_shapefile, SM_dir, include_years = 2, percentile = None, kind = 'scenes', limits = [-180,-90,180,90]):
    '''
    Load ALOS backscatter, plot biomass and soil moisture into a pandas dataframe.
    
    Args:
        alos_dir: Directory (or list of directories) containing preprocessed ALOS data. Filenames need to be structured as output from 'approach preprrocessing chain', i.e. ALOS2_1-1_HV_filter_20180210_ALOS2200906800-180210.tif
        plot_shapefile: Location of a shapefile containing forest plot polygons. Necessary fields are 'plotcode' (unique ID), 'site' (e.g. kilwa), 'country' (e.g. Tanzania), and as many year fields containing AGB figures (tC/ha) as measurements exist (i.e. 2007, 2010, 2017).
        SM_dir: Directory containing ESA CCI soil moisture data
        include_years: The number of years distant from a plot measurement that plot data should be considered valid for
        percentile: Rather than a mean backscatter, return a specified percentile from with each plot.     
    
    Returns:
        A pandas dataframe containing all data for analysis
    '''
            
    # Load ALOS scene metadata
    df = cloudforest.IO.getALOS(alos_dir, kind = kind, limits = limits)

    # Load shapefile metadata
    sf = cloudforest.IO.getShapefile(plot_shapefile)

    # Interpolate and extrapolate AGB values from shapefile
    sf = cloudforest.interpolate.interpolateAGB(sf, include_years = include_years)

    # Prepare output data.frame
    df_out = pd.DataFrame(columns=['filename','sensor','pol','date','hv','hv_db','sm','agb','plotcode','site','country'])
    
    # Get backscatter values
    for index,row in df.iterrows():
        
        # Load plot mask
        plot_mask = cloudforest.IO.maskShapefile(row['filename'], plot_shapefile, location_id = True)
        
        # Skip if no plots within geotiff
        if np.sum(plot_mask>0) == 0: continue
        
        # Load backscatter
        backscatter = cloudforest.IO.loadGeoTiff(row['filename'])
        
        # Load soil moisture estimate
        soil_moisture = cloudforest.SM.getSM(row['filename'], SM_dir, search_days = 7, interpolation = 'nearest')
        
        for plot in np.unique(plot_mask):
            
            if plot == 0: continue
            
            # Extract plot AGB, backscatter and soil moisture
            agb = sf.loc[plot-1][str(row['date'].year)]
            sm = np.mean(soil_moisture[plot_mask == plot])
            
            if percentile is None: hv = np.mean(backscatter[plot_mask == plot])
            if percentile is not None: hv = np.percentile(backscatter[plot_mask == plot], percentile)
            
            # skip if any missing
            if np.isnan(agb) or hv == 0 or np.isfinite(hv)==False: continue #TODO: skip if SM not present
                    
            df_out = df_out.append({'filename':row['filename'],
                                    'sensor':row['sensor'],
                                    'pol':row['pol'],
                                    'date':row['date'],
                                    'doy': (row['date'] - pd.Timestamp('%s-01-01'%str(row['date'].year))).days,
                                    'hv':hv,
                                    'hv_db':10 * np.log10(hv),
                                    'sm':sm,
                                    'agb':agb,
                                    'plotcode':sf.loc[plot-1]['plotcode'],
                                    'site':sf.loc[plot-1]['site'],
                                    'country':sf.loc[plot-1]['country']}, ignore_index = True)
    
    return df_out

    

if __name__ == '__main__':
    '''
    Scripts to test the use of a the Water Cloud Model for improved biomass estimation in the dry tropics.
    
    Environment setup:
    conda create -n cloudforest -c conda-forge python=3.7 tqdm scipy numpy matplotlib pandas scikit-image pillow pyshp gdal ipython
    '''
    
    ####################
    ### Run the code ###
    ####################
    
    # Data inputs (Africa)
    alos_dir = ['/home/sbowers3/DATA/cloudforest_data/gorongosa','/home/sbowers3/DATA/cloudforest_data/kilwa','/home/sbowers3/DATA/cloudforest_data/kruger/','/home/sbowers3/DATA/cloudforest_data/bateke/', '/home/sbowers3/DATA/cloudforest_data/bicuar']
    plot_shapefile = '/home/sbowers3/DATA/cloudforest_data/DATA/plot_data/shp_biom_wgs84.shp' # Forest plot data
    sm_dir = '/home/sbowers3/DATA/soil_moisture' # Directory containing soil moisture data (ESA CCI soil moisture)
    bare_shapefile = '/home/sbowers3/DATA/cloudforest_data/DATA/bare_data/bare_areas.shp' # Bare ground plot data
    kind = 'scenes'
    
    # Data inputs (Australia)
    #alos_dir = '~/SMFM/ALOS_data_australia'
    #plot_shapefile = '/home/sbowers3/DATA/cloudforest/DATA/plot_data/australia/auscover/auscover.shp'
    #kind = 'mosaic'
    limits = [145, -35, 155, -20] # Australia, only active when using mosaic
    
    
    # Load data for bare and vegetated areas
    df_soil = loadData(alos_dir, bare_shapefile, sm_dir, include_years = 12, percentile = 10, limits = limits)
    df_veg = loadData(alos_dir, plot_shapefile, sm_dir, include_years = 3, limits = limits, kind = kind)
    
    # Get parameters C and D, based on backscatter variation at bare locations
    D, C, r_value, p_value, stderr = scipy.stats.linregress(df_soil['sm'], df_soil['hv'])
        
    # Fit water cloud model
    args = (df_veg['hv'], df_veg['sm'], df_veg['agb'],C,D)
    model_wcm11, params_wcm11 = cloudforest.models.optimize_params(cloudforest.models.wcm11_forward, [0.5,0.5], args, bounds = ((0,1),(0,1)), maxiter=50000, method='TNC')
    
    # Fit linear model
    slope, intercept, r_value, p_value, std_err = scipy.stats.linregress(df_veg['agb'],df_veg['hv'])
    
    ##########################
    ### Build output plots ###
    ##########################
    
    output_dir = '/home/sbowers3/DATA/cloudforest/outputs/'
    
    # Plot 0 (bare soil moisture response)
    plt.figure(figsize=[6.4,4.8])
    plt.scatter(df_soil['sm'], df_soil['hv'], c='blue')
    plt.plot(np.arange(0,1,0.01),np.arange(0,1,0.01)*D + C, c='k')
    plt.xlim([0,0.4])
    plt.ylim([0,0.02])
    plt.xlabel('Volumetric soil moisture')
    plt.ylabel('Backscatter')
    plt.savefig('%s/plot_0.png'%output_dir)
    plt.close()
    
    # Plot 1 (linear vs WCM plot, biomass/backscatter)
    plt.figure(figsize=[6.4,4.8])
    plt.scatter(df_veg['agb'], df_veg['hv'], c='k', label='data')
    plt.scatter(df_veg['agb'], cloudforest.models.wcm11_forward(params_wcm11, None, df_veg['sm'], df_veg['agb']),c='green', label='wcm_11')
    plt.scatter(df_veg['agb'], df_veg['agb'] * slope + intercept, c='lightblue', label='linear')
    plt.xlabel('Measured AGB (tC/ha)'); plt.ylabel('Predicted backscatter')
    plt.legend()
    plt.savefig('%s/plot_1.png'%output_dir)
    plt.close()
    
    # Plot 2 (Model prediction comparison)
    plt.figure(figsize=[6.4,4.8])
    plt.scatter(df_veg['hv'], cloudforest.models.wcm11_forward(params_wcm11, None, df_veg['sm'], df_veg['agb']),c='green', label='wcm_11')
    plt.scatter(df_veg['hv'], df_veg['agb'] * slope + intercept, c='lightblue', label='linear')    
    plt.plot(np.arange(0,1,0.1),np.arange(0,1,0.1),c='k', linestyle='--', label = '1:1')
    plt.xlim([0,0.06]); plt.ylim([0,0.06])
    plt.xlabel('Measured backscatter'); plt.ylabel('Predicted backscatter')
    plt.legend()
    plt.savefig('%s/plot_2.png'%output_dir)
    plt.close()
    
    # Plot 3 (Forward vs inverse model)
    plt.figure(figsize=[12.8,4.8])
    
    # Set color map
    norm = mpl.colors.Normalize(vmin=-0.2, vmax=0.6)
    cmap = cm.Blues
    m = cm.ScalarMappable(norm=norm, cmap=cmap)
    
    # and output range
    agb_range = np.arange(0,100,0.1)
    s0_range = np.arange(0,0.08,0.005)
    
    # Visualise forward model
    plt.subplot(121)
    plt.title('Forward model')
    for this_sm in [0.,0.05,0.1,0.2,0.4,0.8]:
        plt.plot(agb_range, cloudforest.models.wcm11_forward(params_wcm11, None, this_sm, agb_range),c=m.to_rgba(this_sm), label = str(this_sm))
    plt.ylim([0,0.08])
    plt.xlim([0,100])
    plt.ylabel('HV backscatter')
    plt.xlabel('AGB (tC/ha)')
    plt.legend()
    
    # Visualise inverse model
    plt.subplot(122)
    plt.title('Inverse model')
    for this_sm in [0.,0.05,0.1,0.2,0.4,0.8]:
        plt.plot(s0_range, cloudforest.models.wcm11_inverse(params_wcm11, s0_range, this_sm,None),c=m.to_rgba(this_sm), label = str(this_sm))
    plt.ylim([0,100])
    plt.xlim([0,0.08])
    plt.xlabel('HV backscatter')
    plt.ylabel('AGB (tC/ha)')
    plt.legend()
    plt.savefig('%s/plot_3.png'%output_dir)
    plt.close()    
    
    
    # Plot 4: Show impact of soil moisture correction on a map

    wet_input = '/home/sbowers3/DATA/cloudforest_data/gorongosa/ALOS2_1-1_HV_filter_20180210_ALOS2200906800-180210.tif'
    dry_input = '/home/sbowers3/DATA/cloudforest_data/gorongosa/ALOS2_1-1_HV_filter_20171104_ALOS2186416800-171104.tif'
    sm_dir = '/home/sbowers3/DATA/soil_moisture'

    wet_sm = cloudforest.SM.getSM(wet_input, sm_dir, search_days = 7, interpolation = 'nearest')
    dry_sm = cloudforest.SM.getSM(dry_input, sm_dir, search_days = 7, interpolation = 'nearest')

    wet_hv = cloudforest.IO.loadGeoTiff(wet_input)
    dry_hv = cloudforest.IO.loadGeoTiff(dry_input)
    wet_hv = np.ma.array(wet_hv, mask = wet_hv == 0)
    dry_hv = np.ma.array(dry_hv, mask = dry_hv == 0)

    # Reproject
    extent = [34, -19.5, 35.5, -18.5]
    wet_hv_rep = cloudforest.IO.reproject(wet_input, wet_hv, extent)
    dry_hv_rep = cloudforest.IO.reproject(dry_input, dry_hv, extent)
    wet_sm_rep = cloudforest.IO.reproject(wet_input, wet_sm, extent)
    dry_sm_rep = cloudforest.IO.reproject(dry_input, dry_sm, extent)
    
    # Get AGB with WCM
    wet_AGB_wcm11 = cloudforest.models.wcm11_inverse(params_wcm11, wet_hv_rep, wet_sm_rep, None, theta = 34.3)
    dry_AGB_wcm11 = cloudforest.models.wcm11_inverse(params_wcm11, dry_hv_rep, dry_sm_rep, None, theta = 34.3)
    
    # Get AGB with linear model
    wet_AGB_lm = (wet_hv_rep  - intercept) / slope
    dry_AGB_lm = (dry_hv_rep  - intercept) / slope
    
    def compare(wet_AGB, dry_AGB):
        '''
        Function to map predicted AGB from two adjacent scenes
        '''
        compare_mask = np.logical_and(wet_AGB.mask, dry_AGB.mask)
        compare_AGB = wet_AGB.data
        compare_AGB[wet_AGB.mask] = dry_AGB.data[wet_AGB.mask]
        compare_AGB = np.ma.array(compare_AGB, mask = compare_mask)
        plt.imshow(compare_AGB, vmin=0, vmax=40)
        plt.colorbar(fraction=0.046, pad=0.04)
   
    plt.figure(figsize=[12.8,4.8])
    
    plt.subplot(121)
    plt.title('WCM')
    compare(wet_AGB_wcm11,dry_AGB_wcm11)
    plt.subplot(122)
    plt.title('Linear model')
    compare(wet_AGB_lm,dry_AGB_lm)
    
    plt.savefig('%s/plot_4.png'%output_dir)
    plt.close()    
    
   