
import numpy as np
import pandas as pd
import scipy.interpolate

import pdb

def _interpolate(agb, years, include_years = 2):
    '''
    Internal function to extrapolate AGB over years.
    
    Args:
        agb: Array of AGB estimates, with np.nan where no inventoruy
        years: Array of years for each estimate or desired estimate
        include_years: Estimate AGB for X years either side of a field measurement. Defaults to 2 years either side of measurement.
    
    Returns:
        AGB interpolated and extrapolated
    '''
        
    # Select values
    s = np.isnan(agb) == False       
        
    # Interpolate and extrapolate
    if s.sum() > 1:
        # Where multiple measurements
        f = scipy.interpolate.interp1d(years[s], agb[s], fill_value = 'extrapolate')
        agb_interp = f(years)
    
    elif s.sum() == 1:
        # Where only one measurement
        agb_interp = np.zeros_like(agb) + agb[s]
    
    else:
        # If no measurements
        agb_interp = np.zeros_like(agb) + np.nan
    
    # Reduce to accepted quality of interpolation (within include_years)
    measurements = np.where(s)[0]
    estimates_include = measurements.copy()
    for i in range(-include_years,include_years+1,1):
        estimates_include = np.unique(np.append(estimates_include, measurements+i))
    
    # Don't extrapolate beyond range of ALOS data availability
    estimates_include = estimates_include[np.logical_or(estimates_include < 0, estimates_include >= years.shape[0]) == False]
    
    # Get years to include
    include_years = years[estimates_include]
    
    agb_interp[np.isin(years, include_years) == False] = np.nan
    
    return agb_interp


def interpolateAGB(sf, years = np.arange(2006,2020,1), include_years = 2):
    '''
    Interpolate and extrapolate AGB between measured years given a shapefile.
    
    Args:
        shapefile: A shapefile containing plot data, including 'year' fields with AGB estimates (format ####)
        years: Years to output AGB estimates
        include_years: Estimate AGB for X years either side of a field measurement. Defaults to 2 years either side of measurement.
    
    Returns:
        A pandas data.frame
        
    '''
    
    # Convert years to array
    years = np.array(years)
    
    # Get years as string
    years_str = years.astype(np.str)
    
    # Add new cols where not present
    for y in years_str:
        if y not in sf:
            sf[y] = np.nan
    
    # For each plot...
    for plot in sf.index:
        
        # Get valid AGB measurements
        agb = sf[years_str].iloc[plot].values
        agb[agb==''] = np.nan
        agb = agb.astype(np.float32)
        
        # Interpolate/Extrapolate
        agb_interp = _interpolate(agb, years, include_years = include_years)
        
        # Add to dataframe
        for n, y in enumerate(years_str):
            sf.loc[sf.index == plot,y] = agb_interp[n]
        
    return sf

