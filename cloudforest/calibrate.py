# TO EDIT!

# Calibrate biomass vs backscatter

import csv
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import scipy.stats
import shapefile

from sklearn.linear_model import LinearRegression 

# Load gamma0 backscatter

for year in [2007,2008,2009,2010,2015,2016,2017]:
    with open('/home/sbowers3/DATA/acacia/DATA/calibration/gamma0_%s_by_plot.csv'%str(year),'rb') as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        header = reader.next()
        gamma0,plot_name = [], []
        for row in reader:
            gamma0.append(float(row[0]))
            plot_name.append(row[3])
    
    this_df = pd.DataFrame({'plot_name':plot_name, 'year':[year]*len(plot_name), 'gamma0':gamma0})
    
    if year == 2007:
        df_gamma0 = this_df.copy()
    else:
        df_gamma0 = df_gamma0.append(this_df)



# Load AGB

shapes = shapefile.Reader('/home/sbowers3/DATA/acacia/DATA/calibration/shp_biom_wgs84.shp')

plotcodes = []

years = [2006,2007,2008,2009,2010,2012,2015,2016,2017,2018]
#year_index = [1,2,3,4,5,6,7,8,9,10]

fields = np.array([i[0] for i in shapes.fields[1:]])

first_time = True
n=-1

for this_shape in shapes.shapeRecords():
    
    for year in years:
                
        if np.array(this_shape.record)[fields==str(year)][0] is None: continue
        
        n+=1
        
        this_df = pd.DataFrame({'plot_name':this_shape.record[np.where(fields=='plotcode')[0][0]], \
                                'year':year, \
                                'AGB':this_shape.record[np.where(fields==str(year))[0][0]], \
                                'site':this_shape.record[np.where(fields=='site')[0][0]], \
                                'country':this_shape.record[np.where(fields=='country')[0][0]]}, index=[n])
        
        if first_time:
            df_agb = this_df.copy()
            first_time = False
        else:
            df_agb = df_agb.append(this_df)

df_agb = df_agb[df_agb['AGB']!=0]



# Calibrate gamma0 to AGB
    
df_gamma0['satellite'] = 'None'
df_gamma0['satellite'][np.logical_and(df_gamma0['year'] >= 2007, df_gamma0['year'] <= 2010)] = 'ALOS-1'
df_gamma0['satellite'][np.logical_and(df_gamma0['year'] >= 2015, df_gamma0['year'] <= 2017)] = 'ALOS-2'

df_gamma0['AGB'] = np.nan
df_gamma0['site'] = ''


for index, row in df_agb.iterrows():
    year = row['year']
    plot_name = row['plot_name']
    agb = row['AGB']
    
    if year < 2007 or year > 2017: continue
    if year > 2010 and year < 2015: continue
    
    df_gamma0['AGB'][np.logical_and(df_gamma0['plot_name']==plot_name, df_gamma0['year'] == year)] = agb
    df_gamma0['site'][np.logical_and(df_gamma0['plot_name']==plot_name, df_gamma0['year'] == year)] = row['site']


df_gamma0['site'][np.isin(df_gamma0['site'], ['lesio','lefini'])] = 'bateke'

# ALOS-1
sel = np.logical_and(df_gamma0.satellite == 'ALOS-1', np.logical_and(np.isnan(df_gamma0.AGB)==False, np.isnan(df_gamma0.gamma0)==False))
a1_g0 = df_gamma0.gamma0[sel]
a1_agb = df_gamma0.AGB[sel]
a1_site = df_gamma0.site[sel]
a1_slope, a1_intercept, a1_r_value, a1_p_value, a1_std_err = scipy.stats.linregress(a1_g0, a1_agb)

# Use sklearn instead (no intercept)
reg = LinearRegression(fit_intercept = False).fit(a1_g0.values.reshape(-1,1), a1_agb.values.reshape(-1,1))
a1_slope = reg.coef_[0]
a1_intercept = reg.intercept_


# ALOS-2
sel = np.logical_and(df_gamma0.satellite == 'ALOS-2', np.logical_and(np.isnan(df_gamma0.AGB)==False, np.isnan(df_gamma0.gamma0)==False))
a2_g0 = df_gamma0.gamma0[sel]
a2_agb = df_gamma0.AGB[sel]
a2_site = df_gamma0.site[sel]
a2_slope, a2_intercept, a2_r_value, a2_p_value, a2_std_err = scipy.stats.linregress(a2_g0, a2_agb)

# Use sklearn instead (no intercept)
reg = LinearRegression(fit_intercept = False).fit(a2_g0.values.reshape(-1,1), a2_agb.values.reshape(-1,1))
a2_slope = reg.coef_[0]
a2_intercept = reg.intercept_


# Both?
#import statsmodels.api as sm
#
#df_model = df_gamma0[['AGB','gamma0','satellite']][np.isnan(df_gamma0.AGB)==False]
#df_model['satellite'] = X.satellite.astype('category')
#
#from statsmodels.formula.api import ols
#from statsmodels.stats.anova import anova_lm

#model = ols('AGB ~ gamma0 + satellite + gamma0*satellite', data=df_model).fit()
#print model.summary()


#y = df_gamma0[['AGB']][np.isnan(df_gamma0.AGB)==False]
#X = sm.add_constant(X)
#est = sm.OLS(y, X).fit()
#est.summary()



#g0 = df_gamma0.gamma0[np.isnan(df_gamma0.AGB)==False]
#agb = df_gamma0.AGB[np.isnan(df_gamma0.AGB)==False]
#satellite = df_gamma0.satellite[np.isnan(df_gamma0.AGB)==False]
#slope, intercept, r_value, p_value, std_err = scipy.stats.linregress(g0, agb)

sites = np.unique(a1_site.tolist() + a2_site.tolist())
markertypes = np.array(['p', '*', 'o', '^', 's', 'd'])    

col_A1 = 'blue'
col_A2 = 'green'

fig, ax = plt.subplots()

label_a1 = r'ALOS-1: AGB = %.2f g$_0$ + %.2f, $R^2$ = %.2f'%(a1_slope,a1_intercept, a1_r_value ** 2)
label_a2 = r'ALOS-2: AGB = %.2f g$_0$ + %.2f, $R^2$ = %.2f'%(a2_slope,a2_intercept, a2_r_value ** 2)

for site, markertype in zip(sites,markertypes):
    ax.scatter(a1_g0[a1_site==site], a1_agb[a1_site==site], facecolors='none', edgecolors=col_A1, s=20, marker = markertype, label = site)
    ax.scatter(a2_g0[a2_site==site], a2_agb[a2_site==site], facecolors='none', edgecolors=col_A2, s=20, marker = markertype, label = '_nolegend_')

ax.plot(np.arange(-100,100,1), np.arange(-100,100,1) * a1_slope + a1_intercept, c = col_A1)
ax.plot(np.arange(-100,100,1), np.arange(-100,100,1) * a2_slope + a2_intercept, c = col_A2)

ax.text(0.002,70, label_a1, color = col_A1)
ax.text(0.002,65, label_a2, color = col_A2)

ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
ax.set_xlim([0,0.1])
ax.set_ylim([0,75])
ax.set_xlabel(r'Radar backscatter ($\gamma_0$, m$^2$/m$^2$)', fontsize=12)
ax.set_ylabel('Aboveground biomass (tC/ha)', fontsize=12)

plt.legend(frameon=False, prop={'size': 9}, loc=4)
plt.show()


"""
# Exponential fit
from scipy.optimize import curve_fit

def func(x, a, b, c):
    return a * np.exp(b * x) - c


a1_params, cov = curve_fit(func, a1_agb, np.log10(a1_g0) * 10)
a2_params, cov = curve_fit(func, a2_agb, np.log10(a2_g0) * 10)

x = np.arange(-10, 100, 0.1)

plt.figure()
plt.scatter(a1_agb, np.log10(a1_g0) * 10, label="ALOS-1")
plt.plot(x, func(x, *a1_params), label="ALOS-1")

plt.scatter(a2_agb, np.log10(a2_g0) * 10, label="ALOS-2")
plt.plot(x, func(x, *a2_params), label="ALOS-2")

plt.legend()
plt.ylim(-20,0)
plt.show()

"""

