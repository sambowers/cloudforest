# Water Cloud Models (inverse)

import math
import numpy as np
import scipy.optimize

"""
# From Qing's script
C = 0.01299755
D = 9.535495e-5

# My guesstimate
C = 0.001
D = 0.06667

# ALOS-2 (80th percentile)
C = -0.00476720991614514
D = 0.11407051000800696

# ALOS-2 (10th percentile)
C = -0.0035234325880575826
D = 0.05183923515840766
"""

# Default: All data (10th percentile)
C = 0.0004968868025296688
D = 0.030691436694140824


######################
### Forward models ###
######################

def attema_forward(wcm_params, sigma0, ms, agb, C = C, D = D, theta = 34.3, element = 'both'):
    '''
    The attema water cloud model
    
    Args:
        wcm_params: List of parameters for the WCM model, in format [A, B]
        sigma0: List of sigma0 values, set None for prediction, or include one per input measurement to return RMSE
        ms: List of soil moisture measurements
        agb: List of AGB measurements
        C: WCM parameter C
        D: WCM parameter D
        theta: angle
        element: 'both', 'vegetation' or 'soil' backscatter output
    
    Returns: Predicted radar backscatter, or RMSE
    '''
    
    assert element in ['vegetation','soil','both'], "Element must be 'vegetation', 'soil' or 'both'."
    
    A = wcm_params[0]
    B = wcm_params[1]
    
    # Convert theta from degrees to radians
    cos_theta = math.cos(math.radians(theta))
    
    s0_veg = A * cos_theta * ( 1 - (np.exp((-B * agb) / cos_theta)))
    s0_soil = C * np.exp((D * ms) + ((-B * agb) / cos_theta)) * cos_theta
    s0_both = s0_veg + s0_soil
    
    s0_out = s0_veg if element is 'vegetation' else s0_soil if element is 'soil' else s0_both
    
    # If minimise return RMSE
    if sigma0 is not None:
        if np.isnan(s0_out).sum() > 1 or np.isinf(s0_out).sum() > 0: rmse = 99999.
        else: rmse = np.sqrt(np.mean((s0_out - sigma0)**2))
        return rmse

    return s0_out

def wcm11_forward(wcm_params, sigma0, ms, agb, C = C, D = D, theta = 34.3, element = 'both'):
    '''
    The WCM_11 water cloud model
    
    Args:
        wcm_params: List of parameters for the WCM model, in format [A, B]
        sigma0: List of sigma0 values, set None for prediction, or include one per input measurement to return RMSE
        ms: List of soil moisture measurements
        agb: List of AGB measurements
        C: WCM parameter C
        D: WCM parameter D
        theta: angle
        element: 'both', 'vegetation' or 'soil' backscatter output
    
    Returns: Predicted radar backscatter, or RMSE
    '''
    
    assert element in ['vegetation','soil','both'], "Element must be 'vegetation', 'soil' or 'both'."
    
    A = wcm_params[0]
    B = wcm_params[1]
        
    # Convert theta from degrees to radians
    cos_theta = math.cos(math.radians(theta))
    B2cos_theta = (2 * B / cos_theta)
    
    s0_veg = A * cos_theta * (1 - np.exp(-B2cos_theta * agb))
    s0_soil = np.exp(-B2cos_theta * agb) * (C + (D * ms))
    s0_both = s0_veg + s0_soil
    
    s0_out = s0_veg if element is 'vegetation' else s0_soil if element is 'soil' else s0_both
    
    # If minimise return RMSE
    if sigma0 is not None:
        if np.isnan(s0_out).sum() > 1 or np.isinf(s0_out).sum() > 0: rmse = 99999.
        else: rmse = np.sqrt(np.mean((s0_out - sigma0)**2))
        return rmse
    
    return s0_out


def wcm1bm_forward(wcm_params, sigma0, ms, agb, C = C, D = D, theta = 34.3, element = 'both'):
    '''
    The WCM_1bm cloud model
    
    Args:
        wcm_params: List of parameters for the WCM model, in format [A, B]
        sigma0: List of sigma0 values, set None for prediction, or include one per input measurement to return RMSE
        ms: List of soil moisture measurements
        agb: List of AGB measurements
        C: WCM parameter C
        D: WCM parameter D
        theta: angle
        element: 'both', 'vegetation' or 'soil' backscatter output
    
    Returns: Predicted radar backscatter, or RMSE
    '''
    
    assert element in ['vegetation','soil','both'], "Element must be 'vegetation', 'soil' or 'both'."
    
    A = wcm_params[0]
    B = wcm_params[1]
    
    # Convert theta from degrees to radians
    cos_theta = math.cos(math.radians(theta))
    B2cos_theta = (2 * B / cos_theta)
    
    s0_veg = A * agb * cos_theta * ( 1 - np.exp(-B2cos_theta * agb))
    s0_soil = np.exp(-B2cos_theta * agb) * (C + (D * ms))
    s0_both = s0_veg + s0_soil
    
    s0_out = s0_veg if element is 'vegetation' else s0_soil if element is 'soil' else s0_both
    
    # If minimise return RMSE
    if sigma0 is not None:
        if np.isnan(s0_out).sum() > 1 or np.isinf(s0_out).sum() > 0: rmse = 99999.
        else: rmse = np.sqrt(np.mean((s0_out - sigma0)**2))
        return rmse
        
    return s0_out

def wcmsimp_forward(wcm_params, sigma0, ms, agb, theta = 34.3, element = 'both'):
    '''
    The 'simple' water cloud model
    
    Args:
        wcm_params: List of parameters for the WCM model, in format [A, B]
        sigma0: List of sigma0 values, set None for prediction, or include one per input measurement to return RMSE
        ms: List of soil moisture measurements
        agb: List of AGB measurements
        C: WCM parameter C
        D: WCM parameter D
        theta: angle
        element: 'both', 'vegetation' or 'soil' backscatter output
    
    Returns: Predicted radar backscatter, or RMSE
    '''
    
    assert element in ['vegetation','soil','both'], "Element must be 'vegetation', 'soil' or 'both'."
    
    A = wcm_params[0]
    B = wcm_params[1]
        
    # Convert theta from degrees to radians
    cos_theta = math.cos(math.radians(theta))
    
    s0_veg = A * cos_theta * (1 - np.exp((-2 * B * agb) / cos_theta))
    s0_soil = 0.
    s0_both = s0_veg + s0_soil
    
    s0_out = s0_veg if element is 'vegetation' else s0_soil if element is 'soil' else s0_both
    
    # If minimise return RMSE
    if sigma0 is not None:
        if np.isnan(s0_out).sum() > 1 or np.isinf(s0_out).sum() > 0: rmse = 99999.
        else: rmse = np.sqrt(np.mean((s0_out - sigma0)**2))
        return rmse
    
    return s0_out


######################
### Inverse models ###
######################

def attema_inverse(wcm_params, sigma0, ms, agb, C = C, D = D, theta = 34.3):
    '''
    The attema water cloud model, inversed to predict AGB
    
    Args:
        wcm_params: List of parameters for the WCM model, in format [A, B]
        sigma0: List of sigma0 values
        ms: List of soil moisture measurements
        agb: List of AGB measurements, set None for prediction, or include one per input measurement to return RMSE
        C: WCM parameter C
        D: WCM parameter D
        theta: angle
        element: 'both', 'vegetation' or 'soil' backscatter output
    
    Returns: Predicted AGB, or RMSE
    '''
    
    A = wcm_params[0]
    B = wcm_params[1]
    
    # Convert theta from degrees to radians
    cos_theta = math.cos(math.radians(theta))
    
    AGB = (-cos_theta / B) * np.log((A - (sigma0 / cos_theta)) / (A - (C * np.exp(D * ms))))
    
    # If minimise return RMSE
    if agb is not None:
        if np.isnan(AGB).sum() > 1 or np.isinf(AGB).sum() > 0: rmse = 99999.
        else: rmse = np.sqrt(np.mean((AGB - agb)**2))
        return rmse
        
    return AGB


def wcm11_inverse(wcm_params, sigma0, ms, agb, C = C, D = D, theta = 34.3):
    '''
    The WCM_11 water cloud model, inversed to predict AGB
    
    Args:
        wcm_params: List of parameters for the WCM model, in format [A, B]
        sigma0: List of sigma0 values
        ms: List of soil moisture measurements
        agb: List of AGB measurements, set None for prediction, or include one per input measurement to return RMSE
        C: WCM parameter C
        D: WCM parameter D
        theta: angle
        element: 'both', 'vegetation' or 'soil' backscatter output
    
    Returns: Predicted AGB, or RMSE
    '''
        
    A = wcm_params[0]
    B = wcm_params[1]
    
    # Convert theta from degrees to radians
    cos_theta = math.cos(math.radians(theta))
    
    AGB = (cos_theta / (-2. * B)) * np.log((sigma0 - (A * cos_theta)) / (C + (D * ms) - (A * cos_theta)))
    
    # If minimise return RMSE
    if agb is not None:
        if np.isnan(AGB).sum() > 1 or np.isinf(AGB).sum() > 0: rmse = 99999.
        else: rmse = np.sqrt(np.mean((AGB - agb)**2))
        return rmse
        
    return AGB
 

def wcm1bm_inverse(wcm_params, sigma0, ms, agb, C = C, D = D, theta = 34.3):
    '''
    The WCM_1bm water cloud model, inversed to predict AGB
    
    Note: This is not at present correct.
    
    Args:
        wcm_params: List of parameters for the WCM model, in format [A, B]
        sigma0: List of sigma0 values
        ms: List of soil moisture measurements
        agb: List of AGB measurements, set None for prediction, or include one per input measurement to return RMSE
        C: WCM parameter C
        D: WCM parameter D
        theta: angle
        element: 'both', 'vegetation' or 'soil' backscatter output
    
    Returns: Predicted AGB, or RMSE
    '''
    
    A = wcm_params[0]
    B = wcm_params[1]
    
    # Convert theta from degrees to radians
    cos_theta = math.cos(math.radians(theta))
    B2cos_theta = ((2 * B / cos_theta))
    CDms = (C + (D * ms))
    
    AGB = ((B2cos_theta * CDms) + (1 * np.sqrt(((B2cos_theta * CDms) ** 2) - (8 * A * B * (CDms - sigma0))))) / (4 * A * B)
    
    # If minimise return RMSE
    if agb is not None: AGB = np.sqrt(np.mean((AGB - agb)**2))
    
    return AGB


def wcmsimp_inverse(wcm_params, sigma0, ms, agb, theta = 34.3):
    '''
    The 'simple' water cloud model, inversed to predict AGB
    
    Args:
        wcm_params: List of parameters for the WCM model, in format [A, B]
        sigma0: List of sigma0 values
        ms: List of soil moisture measurements
        agb: List of AGB measurements, set None for prediction, or include one per input measurement to return RMSE
        C: WCM parameter C
        D: WCM parameter D
        theta: angle
        element: 'both', 'vegetation' or 'soil' backscatter output
    
    Returns: Predicted AGB, or RMSE
    Note: This is not at present correct.
    '''
    
    A = wcm_params[0]
    B = wcm_params[1]
    
    # Convert theta from degrees to radians
    cos_theta = math.cos(math.radians(theta))
    
    AGB = (cos_theta /  (-2 * B) ) * np.log(1 - (sigma0 / (cos_theta * A)))
    
    # If minimsse return RMSE
    if agb is not None: AGB = np.sqrt(np.mean((AGB - agb)**2))
    
    return AGB


####################
### Optimisation ###
#################### 
 
def optimize_params(model, param_guess, args, bounds = ((0,1),(0,1)), maxiter=15000, maxfun = 15000, method='TNC'):
    '''
    Wrapper function to perform model optimisation
    
    Args:
        model: The Water Cloud Model to optimize
        param_guess: List of parameter guesses in form [A, B]
        args: Tuple of input arguments for model
        bounds: Limits of paramter values A and B. Defaults to 0-1 for A and B.
    
    Returns:
        An optimised model
        Optimised model parameters
        
    '''
    
    param_guess = np.array(param_guess)
    
    model_opt = scipy.optimize.minimize(model, param_guess, args=args, bounds = bounds, method=method, options={'disp':True, 'maxiter':maxiter})
    
    return model_opt, model_opt['x']
