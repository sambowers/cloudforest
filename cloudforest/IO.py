# Input/Output functions

import datetime
import glob
import numpy as np
from osgeo import gdal, gdalnumeric, ogr, osr
import pandas as pd
from PIL import Image, ImageDraw
import shapefile

import pdb

##############
### Pandas ###
##############

def getALOS(data_dir, kind = 'scenes', limits = [-180,-90,180,90]):
    '''
    Takes a directory as input, searches and extracts metadata for all preprocessed ALOS scenes. Scenes must follow the 'approach' filename format.
    
    Args:
        data_dir: Directory containing data
    Returns:
        A pandas data.frame
    '''
    
    if type(data_dir) != list: data_dir = [data_dir]
    
    assert kind is 'scenes' or kind is 'mosaic', "'kind' must be either scenes or mosaic'."
        
    df = pd.DataFrame(columns=['filename','sensor','pol','date'])
    
    # For scenes
    if kind == 'scenes':
        
        for directory in data_dir:
            
            files = sorted(glob.glob('%s/ALOS?_?-?_HV_*.tif'%directory))

            for this_file in files:
                
                f = this_file.split('/')[-1]
                
                sensor = f.split('_')[0]
                pol = f.split('_')[1]
                date = datetime.datetime.strptime(f.split('_')[4], '%Y%m%d')
                
                df = df.append({'filename':this_file,'sensor':sensor,'pol':pol,'date':date} , ignore_index=True)
    
    # Ugly solution, but allows for loading of ALOS mosaic product. Set limits appropriately for speed
    else:
        
        try:
            import biota
        except:
            print("You'll need to have the 'biota' library installed to use the ALOS mosaic.")
            raise
        
        # Find all present tiles
        for lat in range(limits[1],limits[3],1):
            for lon in range(limits[0],limits[2],1):
                for year in np.arange(2007,2011,1).tolist() + np.arange(2015,2018,1).tolist():
                    try:
                        tile = biota.LoadTile(data_dir[0],lat,lon,year)
                    except:
                        continue
                    
                    sensor = tile.satellite
                    pol = 'HV'
                    date = datetime.datetime(year,1,1)
        
                    df = df.append({'filename':'%s___%s___%s___%s'%(data_dir[0],str(lat),str(lon),str(year)),
                                    'sensor':sensor,'pol':pol,'date':date} , ignore_index=True)
    
    return df


def getShapefile(plot_shapefile):
    '''
    Extract info from shapefile into pandas data.frame

    Based on: https://www.adamerispaha.com/2017/01/24/reading-shapefiles-into-pandas-dataframes/
    
    Args:
        plot_shapefile: Location of a shapefile
    Returns:
        A pandas data.frame, with one row per shape and one col per field
    '''
    
    sf = shapefile.Reader(plot_shapefile)
    
    fields = [x[0] for x in sf.fields][1:]
    records = sf.records()
    shps = [s.points for s in sf.shapes()]
    
    shapefile_dataframe = pd.DataFrame(columns=fields, data=records)
    
    shapefile_dataframe = shapefile_dataframe.assign(coords=shps)
    
    return shapefile_dataframe


###############
### GeoTiff ###
###############

def loadGeoTiff(filename):
    '''
    Load a GeoTiff file into memory with GDAL
    
    args:
        filename: Path to GeoTiff file
    returns:
        A numpy array
    '''
    
    # In case of scenes
    if filename.split('.')[-1] == 'tif':
        ds = gdal.Open(filename, 0)
        data = ds.ReadAsArray()
    
    # In case of mosaic
    else:
        import biota
        
        data_dir, lat, lon, year = filename.split('___')
        tile = biota.LoadTile(data_dir, int(lat), int(lon), int(year), lee_filter = True)
        
        data = tile.getGamma0()
        
    return data


def reproject(filename, array, extent):
    '''
    Load a version of ALOS data with a different extent
    '''
    
    from osgeo import gdal
           
    assert len(extent) == 4, "Extent must be in the form [xmin, ymin, xmax, ymax]."
    
    ds = gdal.Open(filename, 0)
    gdal_driver = gdal.GetDriverByName('MEM')
    
    # Define output metadata
    res = 1./4500
    nrows = np.int(np.round((extent[3] - extent[1]) / res))
    ncols = np.int(np.round((extent[2] - extent[0]) / res))
    geo_t = (extent[0], res, 0, extent[3], 0, -res)
    proj = ds.GetProjection()
    
    # Build 'from' dataset
    ds_in = gdal_driver.Create('', ds.RasterXSize, ds.RasterYSize, 1, 6)
    ds_in.SetGeoTransform(ds.GetGeoTransform())
    ds_in.SetProjection(proj)
    ds_in.GetRasterBand(1).SetNoDataValue(array.fill_value)
    ds_in.GetRasterBand(1).WriteArray(array.filled())

    # Create 'to' dataset
    ds_out = gdal_driver.Create('', ncols, nrows, 1, 6)
    ds_out.SetGeoTransform(geo_t)
    ds_out.SetProjection(proj)
    ds_out.GetRasterBand(1).SetNoDataValue(array.fill_value)    
    ds_out.GetRasterBand(1).WriteArray(np.zeros((nrows,ncols)) + array.fill_value)
    
    # Reproject and load
    gdal.ReprojectImage(ds_in, ds_out, proj, proj, 0)
    data = ds_out.GetRasterBand(1).ReadAsArray()
    
    data = np.ma.array(data, mask = data == array.fill_value)
    
    return data
    


#################
### Shapefile ###
#################

def _coordinateTransformer(shp, EPSG = 4326):
    """
    Generates function to transform coordinates from a source shapefile CRS to EPSG code.

    Args:
        shp: Path to a shapefile.

    Returns:
        A function that transforms shapefile points to EPSG code (default = 4326).
    """
    
    driver = ogr.GetDriverByName('ESRI Shapefile')
    ds = driver.Open(shp)
    layer = ds.GetLayer()
    spatialRef = layer.GetSpatialRef()

    # Create coordinate transformation
    inSpatialRef = osr.SpatialReference()
    inSpatialRef.ImportFromWkt(spatialRef.ExportToWkt())

    outSpatialRef = osr.SpatialReference()
    outSpatialRef.ImportFromEPSG(EPSG)

    coordTransform = osr.CoordinateTransformation(inSpatialRef, outSpatialRef)

    return coordTransform


def _world2Pixel(geo_t, x, y):
    """
    Uses a gdal geomatrix (ds.GetGeoTransform()) to calculate the pixel location of a geospatial coordinate.
    Modified from: http://geospatialpython.com/2011/02/clip-raster-using-shapefile.html.

    Args:
        geo_t: A gdal geoMatrix (ds.GetGeoTransform().
        x: x coordinate in map units.
        y: y coordinate in map units.
        
    Returns:
        A tuple with pixel/line locations for each input coordinate.
    """
    ulX = geo_t[0]
    ulY = geo_t[3]
    xDist = geo_t[1]
    yDist = geo_t[5]

    pixel = int(round((x - ulX) / xDist))
    line = int(round((y - ulY) / yDist))

    return (pixel, line)


def maskShapefile(geotiff, shp, location_id = False):
    """
    Rasterize points, lines or polygons from a shapefile to match ALOS mosaic data.

    Args:
        geotiff: path to a GeoTiff file
        shp: Path to a shapefile consisting of points, lines and/or polygons. This does not have to be in the same projection as ds
        location_id: Set True to return a unique ID for each masked shape. Note: This is not zero indexed, but starts at 1.

    Returns:
        A numpy array with a boolean (or integer) mask delineating locations inside and outside the shapefile.
    """
    
    if geotiff.split('.')[-1] != 'tif':
        
        import biota
        
        data_dir, lat, lon, year = geotiff.split('___')
        
        print(lat,lon)
        
        tile = biota.LoadTile(data_dir, int(lat), int(lon), int(year))
        geo_t = tile.geo_t
        RasterXSize = tile.xSize
        RasterYSize = tile.ySize
        
    else:
        # Open geotiff file
        ds = gdal.Open(geotiff)
        geo_t = ds.GetGeoTransform()
        RasterXSize = ds.RasterXSize
        RasterYSize = ds.RasterYSize
    
    # Create output image. Add a buffer around the image array equal to the maxiumum dilation size. This means that features just outside ALOS tile extent can contribute to dilated mask.
    rasterPoly = Image.new("I", (RasterXSize, RasterYSize), 0)
    rasterize = ImageDraw.Draw(rasterPoly)

    # The shapefile may not have the same CRS as ALOS mosaic data, so this will generate a function to reproject points.
    coordTransform = _coordinateTransformer(shp)
    
    # Read shapefile
    s = shapefile.Reader(shp)

    # Get shapes
    shapes = np.array(s.shapes())
    
    # For each shape in shapefile...
    for n, shape in enumerate(shapes):

        # Get shape bounding box
        if shape.shapeType == 1 or shape.shapeType == 11:
            # Points don't have a bbox, calculate manually
            sxmin = np.min(np.array(shape.points)[:,0])
            sxmax = np.max(np.array(shape.points)[:,0])
            symin = np.min(np.array(shape.points)[:,1])
            symax = np.max(np.array(shape.points)[:,1])
        else:
            sxmin, symin, sxmax, symax = shape.bbox

        # Transform bounding box points
        sxmin, symin, z = coordTransform.TransformPoint(sxmin, symin)
        sxmax, symax, z = coordTransform.TransformPoint(sxmax, symax)

        # Go to the next record if out of bounds
        if sxmax < geo_t[0]: continue
        if sxmin > geo_t[0] + (geo_t[1] * RasterXSize): continue
        if symax < geo_t[3] + (geo_t[5] * RasterYSize): continue
        if symin > geo_t[3]: continue

        # Separate polygons with list indices
        n_parts = len(shape.parts) #Number of parts
        indices = shape.parts #Get indices of shapefile part starts
        indices.append(len(shape.points)) #Add index of final vertex

        # Catch to allow use of point shapefiles, which don't have parts
        if shape.shapeType == 1 or shape.shapeType == 11:
            n_parts = 1
            points = shape.points

        for part in range(n_parts):

            if shape.shapeType != 1 and shape.shapeType != 11:

                start_index = shape.parts[part]
                end_index = shape.parts[part+1]
                points = shape.points[start_index:end_index] #Map coordinates

            pixels = [] #Pixel coordinantes

            # Transform coordinates to pixel values
            for p in points:

                # First update points from shapefile projection to ALOS mosaic projection
                x, y, z = coordTransform.TransformPoint(p[0], p[1])

                # Then convert map to pixel coordinates using geo transform
                pixels.append(_world2Pixel(geo_t, x, y))

            # Draw the mask for this shape...
            # if a point...
            if shape.shapeType == 0 or shape.shapeType == 1 or shape.shapeType == 11:
                rasterize.point(pixels, n+1)

            # a line...
            elif shape.shapeType == 3 or shape.shapeType == 13:
                rasterize.line(pixels, n+1)

            # or a polygon.
            elif shape.shapeType == 5 or shape.shapeType == 15:
                rasterize.polygon(pixels, n+1)

            else:
                print('Shapefile type %s not recognised!'%(str(shape.shapeType)))

    #Converts a Python Imaging Library array to a gdalnumeric image.
    mask = gdalnumeric.frombuffer(rasterPoly.tobytes(),dtype=np.uint32)
    mask.shape = rasterPoly.im.size[1], rasterPoly.im.size[0]
    
    if location_id == False:
        
        # Get rid of record numbers
        mask = mask > 0

    return mask

