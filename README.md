# CloudForest

Scripts to calibrate and apply a Water Cloud Model to model aboveground biomass in ALOS PALSAR data

## Requirements:
* ALOS PALSAR/PALSAR-2 data (level 1.1/1.5), terrain corrected. I used method in https://bitbucket.org/sambowers/approach/, but other software could be used.
* Forest plot data, in a shapefile. Not included here as not all data belong to UoE or free to share.
* Soil moisture data, from ESA CCI at https://www.esa-soilmoisture-cci.org/
* Python 3, with libraries including pyshp, osgeo (gdal, ogr, osr), pandas, scipy, numpy, PIL.

## Contact:
If you give this a try yourself, it's strongly recommended to first contact sam.bowers@ed.ac.uk for instruction!

