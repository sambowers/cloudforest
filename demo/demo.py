# Demo for fitting the water cloud model

import matplotlib as mpl
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import numpy as np
import os
import pandas as pd
import scipy.stats
import sys

## Hacky import to avoid setup of cloudforest. Alternative is to 'python setup.py install'
sys.path.append(os.path.join(os.getcwd(), '..'))
import cloudforest.models



############################################################
## Step 1: Model backscatter sensitivity to soil moisture ##
############################################################

# Load sample data from a series of 'bare' locations (0 tC/ha AGB, signal dominated by soil moisture variation)
df_soil = pd.read_csv('df_soil.csv')

# Get parameters C and D, based on backscatter variation at bare locations
D, C, r_value, p_value, stderr = scipy.stats.linregress(df_soil['sm'], df_soil['hv'])


#######################################
## Step 2: Fit the water cloud model ##
#######################################

# Load sample data from large (> 0.5 ha) forest plots of varied AGB, and multiple ALOS overpasses for each.
df_veg = pd.read_csv('df_veg.csv')
df_veg_aus = pd.read_csv('df_veg_aus.csv')

# Fit water cloud model to determine parameters A and B
initial_guess = [0.5, 0.5] # A and B, initial guess
bounds = ((0,1),(0,1)) # Min and max of A and B, respectively
wc_model = cloudforest.models.wcm11_forward # Pick a flavour of the water cloud model
wc_model_inverse = cloudforest.models.wcm11_inverse # ...and its inverse version
args = (df_veg['hv'], df_veg['sm'], df_veg['agb'],C ,D) # Inputs to model

# Fit the model
optimised_model, wc_params = cloudforest.models.optimize_params(wc_model, initial_guess, args, bounds)

# Repeat for Australia
args_aus = (df_veg_aus['hv'], df_veg_aus['sm'], df_veg_aus['agb'],C ,D) # Inputs to model
optimised_model_aus, wc_params_aus = cloudforest.models.optimize_params(wc_model, initial_guess, args_aus, bounds)


###############################################################
## Step 2b: Find the regions where params fit aus and africa ##
###############################################################

A_min, A_max, A_step = 0.001, 1., 0.005
B_min, B_max, B_step = 0.0001, 0.1, 0.0005

A_range = slice(A_min,A_max,A_step)
B_range = slice(B_min,B_max,B_step)

wc_params_brute, rmse_final, searchgrid, rmse = scipy.optimize.brute(
    cloudforest.models.wcm11_forward,
    (A_range,B_range),
    args=args, finish = None, workers = 20, full_output = True)                                                        

wc_params_brute_aus, rmse_final_aus, searchgrid_aus, rmse_aus = scipy.optimize.brute(
    cloudforest.models.wcm11_forward,
    (A_range,B_range),
    args=args_aus, finish = None, workers = 20, full_output = True)


param_region = (rmse < rmse.min() * 1.1) * 1 + (rmse < rmse.min() * 1.25) * 1
param_region_aus = (rmse_aus < rmse_aus.min() * 1.1) * 1 + (rmse_aus < rmse_aus.min() * 1.25) * 1

param_region = np.ma.array(rmse, mask = (rmse>rmse.min()*1.5))
param_region_aus = np.ma.array(rmse_aus, mask = (rmse_aus>rmse_aus.min()*1.5))

# Plot it

fig = plt.figure(figsize=[12.8,4.8])

ax1 = plt.subplot(1,2,1)
ax2 = plt.subplot(1,2,2)

im1 = ax1.imshow(param_region,extent=[-1,1,-1,1],vmin=0.007,vmax=0.012)
ax1.set_yticks([1, 0.5, 0., -0.5, -1])
ax1.set_yticklabels(['0.00', '0.25','0.5','0.75', '1.00'])
ax1.set_xticks([-1, -0.5, 0., 0.5, 1])
ax1.set_xticklabels(['0.00','0.025','0.05','0.075', '0.10'])
ax1.set_ylabel('A value')
ax1.set_xlabel('B value')
ax1.set_title('Africa (temporal)')

im2 = ax2.imshow(param_region_aus,extent=[-1,1,-1,1],vmin=0.007,vmax=0.012)
ax2.set_yticks([1, 0.5, 0., -0.5, -1])
ax2.set_yticklabels(['0.00', '0.25','0.5','0.75', '1.00'])
ax2.set_xticks([-1, -0.5, 0., 0.5, 1])
ax2.set_xticklabels(['0.00','0.025','0.05','0.075', '0.10'])
ax2.set_ylabel('A value')
ax2.set_xlabel('B value')
ax2.set_title('Australia (spatial)')

ax1.scatter(((wc_params[1] / B_max) * 2) - 1, 1 - ((wc_params[0] / A_max) * 2), label = 'Africa optimum')
ax1.scatter(((wc_params_aus[1] / B_max) * 2) - 1, 1 - ((wc_params_aus[0] / A_max) * 2), label = 'Australia optimum')

ax2.scatter(((wc_params[1] / B_max) * 2) - 1, 1 - ((wc_params[0] / A_max) * 2), label = 'Africa optimum')
ax2.scatter(((wc_params_aus[1] / B_max) * 2) - 1, 1 - ((wc_params_aus[0] / A_max) * 2), label = 'Australia optimum')

ax1.legend(loc=4)
ax2.legend(loc=4)

plt.show()

##############################################################
## Step 3: Fit an old-fasioned linear model, for comparison ##
##############################################################

# Fit a simple linear model, for comparison
slope, intercept, r_value, p_value, std_err = scipy.stats.linregress(df_veg['agb'],df_veg['hv'])
slope_aus, intercept_aus, r_value_aus, p_value_aus, std_err_aus = scipy.stats.linregress(df_veg_aus['agb'],df_veg_aus['hv'])

##########################
## Step 4: Plot outputs ##
##########################

# Show 'bare' plots, showing backscatter reponse to variations in soil moisture without vegetation
plt.figure(figsize=[6.4,4.8])
plt.scatter(df_soil['sm'], df_soil['hv'], c='blue')
plt.plot(np.arange(0,1,0.01),np.arange(0,1,0.01)*D + C, c='k')
plt.xlim([0,0.4])
plt.ylim([0,0.02])
plt.xlabel('Volumetric soil moisture')
plt.ylabel('Backscatter')
plt.show()

#########################
#########################

# Compare the biomass/backscatter relations with linear model vs water cloud model
plt.figure(figsize=[6.4,4.8])
plt.title('Africa')
plt.scatter(df_veg['agb'], df_veg['hv'], c='k', label='data')
plt.scatter(df_veg['agb'], wc_model(wc_params, None, df_veg['sm'], df_veg['agb']),c='green', label='WCM')
plt.scatter(df_veg['agb'], df_veg['agb'] * slope + intercept, c='lightblue', label='linear')
plt.xlabel('Measured AGB (tC/ha)'); plt.ylabel('Predicted backscatter')
plt.legend()
plt.show()

plt.figure(figsize=[6.4,4.8])
plt.title('Australia')
plt.scatter(df_veg_aus['agb'], df_veg_aus['hv'], c='k', label='data')
plt.scatter(df_veg_aus['agb'], wc_model(wc_params_aus, None, df_veg_aus['sm'], df_veg_aus['agb']),c='green', label='WCM')
plt.scatter(df_veg_aus['agb'], df_veg_aus['agb'] * slope_aus + intercept_aus, c='lightblue', label='linear')
plt.xlabel('Measured AGB (tC/ha)'); plt.ylabel('Predicted backscatter')
plt.legend()
plt.show()

#########################
#########################

agb_test = df_veg['agb']
hv_test = df_veg['hv']
hv_wcm_pred = cloudforest.models.wcm11_forward(wc_params, None, df_veg['sm'], df_veg['agb'])
hv_lin_pred = df_veg['agb'] * slope + intercept
rmse_wcm = (((hv_wcm_pred - hv_test) ** 2).mean()**0.5).round(5)
rmse_linear = (((hv_lin_pred - hv_test) ** 2).mean()**0.5).round(5)

# Compare the prediction of the water cloud model vs the linear model
plt.figure(figsize=[6.4,4.8])
plt.title('Africa')
plt.scatter(df_veg['hv'], wc_model(wc_params, None, df_veg['sm'], df_veg['agb']),c='green', label='WCM, RMSE: %s'%str(rmse_wcm))
plt.scatter(df_veg['hv'], df_veg['agb'] * slope + intercept, c='lightblue', label='Linear, RMSE: %s'%str(rmse_linear))
plt.plot(np.arange(0,1,0.1),np.arange(0,1,0.1),c='k', linestyle='--', label = '1:1')
plt.xlim([0,0.06]); plt.ylim([0,0.06])
plt.xlabel('Measured backscatter'); plt.ylabel('Predicted backscatter')
plt.legend()
plt.show()


agb_test = df_veg_aus['agb']
hv_test = df_veg_aus['hv']
hv_wcm_pred = cloudforest.models.wcm11_forward(wc_params_aus, None, df_veg_aus['sm'], df_veg_aus['agb'])
hv_lin_pred = df_veg_aus['agb'] * slope_aus + intercept_aus
rmse_wcm = (((hv_wcm_pred - hv_test) ** 2).mean()**0.5).round(5)
rmse_linear = (((hv_lin_pred - hv_test) ** 2).mean()**0.5).round(5)

# Compare the prediction of the water cloud model vs the linear model
plt.figure(figsize=[6.4,4.8])
plt.title('Australia')
plt.scatter(df_veg_aus['hv'], wc_model(wc_params_aus, None, df_veg_aus['sm'], df_veg_aus['agb']),c='green', label='WCM, RMSE: %s'%str(rmse_wcm))
plt.scatter(df_veg_aus['hv'], df_veg_aus['agb'] * slope_aus + intercept_aus, c='lightblue', label='Linear, RMSE: %s'%str(rmse_linear))
plt.plot(np.arange(0,1,0.1),np.arange(0,1,0.1),c='k', linestyle='--', label = '1:1')
plt.xlim([0,0.06]); plt.ylim([0,0.06])
plt.xlabel('Measured backscatter'); plt.ylabel('Predicted backscatter')
plt.legend()
plt.show()

#########################
#########################

# Show the forward water cloud model, and its inverse

# Set color map
norm = mpl.colors.Normalize(vmin=-0.2, vmax=0.6)
cmap = cm.Blues
m = cm.ScalarMappable(norm=norm, cmap=cmap)

# and output range
agb_range = np.arange(0,100,0.1)
s0_range = np.arange(0,0.08,0.005)

plt.figure(figsize=[12.8,4.8])

# Visualise forward model
plt.subplot(121)
plt.title('Forward model')
for this_sm in [0.,0.05,0.1,0.2,0.4,0.8]:
    plt.plot(agb_range, wc_model(wc_params, None, this_sm, agb_range),c=m.to_rgba(this_sm), label = str(this_sm))
plt.plot(agb_range,agb_range*slope + intercept,c='lightblue',linestyle='--', label = 'linear')
plt.ylim([0,0.08])
plt.xlim([0,100])
plt.ylabel('HV backscatter')
plt.xlabel('AGB (tC/ha)')
plt.legend()

# Visualise inverse model
plt.subplot(122)
plt.title('Inverse model')
for this_sm in [0.,0.05,0.1,0.2,0.4,0.8]:
    plt.plot(s0_range, wc_model_inverse(wc_params, s0_range, this_sm,None),c=m.to_rgba(this_sm), label = str(this_sm))
plt.plot(agb_range*slope + intercept, agb_range,c='lightblue',linestyle='--', label = 'linear')

plt.ylim([0,100])
plt.xlim([0,0.08])
plt.xlabel('HV backscatter')
plt.ylabel('AGB (tC/ha)')
plt.legend()

plt.show() 
    
